from bs4 import BeautifulSoup
import requests
import json
from modules.lumodule import *


lu = lu()


# Functions enables us to grab a websites HTML
def getHtml(url):
	# Assign the returned data to the variable 'soup' and
	# get it ready for parsing with BeautifulSoup & LXML
	soup = BeautifulSoup(requests.get(url).text, 'lxml')
	return soup


# Function to parse sitekey from HTML
# Takes a passing variable as 'html'
def parseSitekey(html):
	
	# Assign the value returned from the scrape to 'sitekey' if it is 
	# found otherwise assign "No Sitekey Found" to the variable instead.
	
	# if a 'div' with the 'class' 'g-recaptcha ' is found within the html, then set 'sitekey' to the
	# found item. The ' ' in {'class':'g-recaptcha '} is vital else the div/element will not be located.
	sitekey = html.find('div', {'class': 'g-recaptcha '})['data-sitekey'] if html.find('div', {'class': 'g-recaptcha '}) else "No Sitekey found"
	return sitekey





if __name__ == '__main__':
	# Clear the console
	console.clear()
	# lu.cookie enables the output to be printer in cyan
	# A fair amount of nested functions, cleans the code
	# up since we're only returning one item essentially
	# and will not need to reuse any variables returned
	# from functions again.
	lu.cookie(parseSitekey(getHtml(str(input("Website: ").strip()))))

