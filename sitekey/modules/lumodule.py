# -*- coding: utf-8 -*-

from colorama import Style, Fore, Back, init
from datetime import datetime

import json
import sys
import os

# Import colorama used for printing
# colored terminal messages and
# initialise style resetting
import colorama
init(autoreset=True)


# To only import if you're on Windows
# so it doesn't throw an error or make
# you install ToastNotifier on any other
# operating system
if sys.platform == "win32":
	from win10toast import ToastNotifier






# Functions for console
class console:
    def clear():
        if sys.platform == "win32":
            # Windows takes one attempt
            # to clear the console
            os.system('cls')
        elif sys.platform == "darwin":
            # whereas OSX takes about 3 
            # attempts to clear terminal
            for c in range(3):
                os.system('clear')
        else:
            pass






# Functions to allow simple OS notifications
class notifs:
    def notif(notif_text, notif_title, notif_ico=None, notif_dura=2):
        if sys.platform == "win32":
            notif = ToastNotifier()
            notif.show_toast(
                                notif_title,
                                notif_text,
                                notif_ico,
                                notif_dura
                            )
        elif sys.platform == "darwin":
            os.system("""osascript -e 'display notification "{0}" with title "{1}"'""".format(notif_text, notif_title))
        else:
            pass


    def osxNotif(notif_text, notif_title):
        os.system("""osascript -e 'display notification "{0}" with title "{1}"'""".format(notif_text, notif_title))


    def winNotif(notif_title, notif_text):
        notif = ToastNotifier()
        notif.show_toast(notif_title, notif_text)





# Functions to simplify terminal logging
class lu():
    def __init__(self):
        pass


    # Allows a timestamp on user input
    def time(self):
        stamp = str(datetime.now().strftime("%H:%M:%S.%f")[:-3])
        return stamp


    def brdim(self, value):
        color = Style.DIM + Style.BRIGHT
        print(color + "{0}".format(value))


    def mask(self, value):
        color = Fore.MAGENTA
        print(color + "{0}".format(value))


    def version(self, value):
        color = Style.BRIGHT + Fore.MAGENTA
        print(color + "Version: {0}".format(value))

    
    def server(self, value):
        color = Style.DIM + Style.BRIGHT + Fore.BLUE
        print(color + "[{0}] [SRVER] » {1}".format(self.time(), value))


    def error(self, value):
        color = Fore.RED
        print(color + "[{0}] [ERROR] » {1}".format(self.time(), value))


    def info(self, value):
        color = Fore.YELLOW
        print(color + "[{0}] [INFRM] » {1}".format(self.time(), value))


    def success(self, value):
        color = Fore.GREEN
        print(color + "[{0}] [VALID] » {1}".format(self.time(), value))


    def cookie(self, value):
        color = Fore.CYAN
        print(color + "[{0}] [COOKI] » {1}".format(self.time(), value))


    def status(self, value):
        color = Fore.MAGENTA
        print(color + "[{0}] [STATS] » {1}".format(self.time(), value))


    def value(self, value):
        color = Style.BRIGHT + Fore.BLUE
        print(color + "[{0}] [VALUE] » {1}".format(self.time(), value))


    def warn(self, value):
        color = Style.BRIGHT + Back.RED + Style.BRIGHT + Fore.YELLOW
        print(color + "[{0}] [WARNN] » {1}".format(self.time(), value))


    def resCode(self, request):
        lu.status(self, "Code: {0} | {1}".format(request.status_code, request.reason))
        return request.status_code


    def monkiJD(self, value):
        color 	= Style.BRIGHT + Fore.BLUE
        try:
            print(color + "[{0}] [mkiJD] » {1}".format(self.time(), json.dumps(json.loads(value), indent=4, ensure_ascii = False)))
        except:
            print(color + "[{0}] [mkiJD] » {1}".format(self.time(), json.dumps(value, indent=4, ensure_ascii = False)))