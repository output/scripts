# -*- coding: UTF-8 -*-

from modules.lumodule import *
from bs4 import BeautifulSoup
import requests


# Initiate the lu()
# class in lumodule
lu = lu()


# Get the page html from bitskins
def getHtml(url):

	# Assign the return data to the 'response' variable
	response = requests.get(url)

	# Assign the souped html to 'soup'
	soup = BeautifulSoup(response.content, 'lxml')
	
	# And return the variable
	return soup


# Function to parse item data
# from HTML obtained from getHtml
def parseInspect(index):

	# Page counter
	iteration = 0

	# Open the file ready to write the item data to it.
	# Encoding is set to utf-8 otherwise when attempting
	# to write to the file as charmap cannot encode '\u2605'
	# which is the star '★' symbol used in glove/knife names
	with open('bitskins_items.txt', 'a', encoding="utf-8") as file:

		# Iterate through Bitskins pages,
		# while the amount of iterations
		# is less than the amount of pages
		while (iteration < index):

			# Printing to the console was only used when trying to debug pages that errors occurred on, such as
			# "charmap' codec can't encode character '\u2605' in position 0: character maps to <undefined>",
			# or "NoneType' object has no attribute 'text'", which relates to no float being able to be found
			lu.info("Page https://bitskins.com/?appid=730&page={0}".format(iteration))

			# Call the getHhtml function and return it BeautifulSouped
			html = getHtml("https://bitskins.com/?appid=730&page={0}".format(iteration))

			# For every 'div' containing the 'item-solo' class in the html, which denotes
			# separate items on the page
			for a in html.find_all('div', {'class':'item-solo'}):

				# Find and assign the below variables using BeautifulSoup to parse them
				item_name = a.find('div', {'class': 'item-title'}).text
				item_icon = a.find('div', {'class': 'item-icon'})['data-src']

				# Find the float of the item if it has one
				item_phase = a.find('p', {'style':'margin: 0; padding: 0px; font-weight: 700; height: 1.5em; font-size: 1.1em; margin-top: -15px;'}).text if a.find('p', {'style':'margin: 0; padding: 0px; font-weight: 700; height: 1.5em; font-size: 1.1em; margin-top: -15px;'}) else "No Phase"

				# Not every item has a float, such as stickers, so if it cannot find the span
				# containing the 'unwrappable-float-pointer in the item's div, then set float
				# to the string "No Float"
				item_float = a.find('span', {'class': 'unwrappable-float-pointer'}).text.split(',')[0] if a.find('span', {'class': 'unwrappable-float-pointer'}) else "No Float"
				item_condition = a.find('div', {'class': 'item-sub-info'}).text.strip().split('/')[0]

				# item_float, item_rarity and item_seed are all located within the same line
				# of text, so most likely the easiest way to parse these out is to use .split()
				# and then use an iterative selector.
				item_rarity = a.find('div', {'class': 'item-sub-info'}).text.strip().split('/')[1].split(' ')[0]
				item_price = a.find('span', {'class': 'item-price-display'}).text

				# The best way to find the item's suggested price is to find the div which contains
				# the said style and a href, instead of finding every <a> tag containing a href of
				# which also has "/price/?market_hash_name=" inside of it
				item_suggested_price = a.find('a', style="color: inherit;", href=True).text

				# Similar to items not having a float, not every item has a paint seed, such
				# as an 'Ultraviolet Falchion Knife', so if it cannot find a number in the
				# 'item-sub-info' div then there is a high possibility it does not have a seed
				item_seed = a.find('div', {'class': 'item-sub-info'}).text.strip().split('/')[2].replace(' ','') if any(str(num) in a.find('div', {'class': 'item-sub-info'}).text for num in [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) else "No Seed"

				# The four btn's in the item div have non-unique identifiers so the best way
				# to select them 'inspect item' btn is to use an iterative selector '[1]'
				item_inspect = a.find_all('a', {'class': 'btn-default'})[1]['href']
				print()

				# Using .format() looks cleaner in this case and keeps everything in a single line as
				# opposed to writing each print() and file.write() on separate lines
				lu.cookie("{0}\n{1}\n{2}\n{3}\n{4}{5}\n{6} (Price)\n{7} (Suggested Price)\n{8}\n{9}\n\n".format(item_name, item_icon, item_phase, item_float, item_condition, item_rarity, item_price, item_suggested_price, item_seed, item_inspect))
				file.write("{0}\n{1}\n{2}\n{3}\n{4}{5}\n{6} (Price)\n{7} (Suggested Price)\n{8}\n{9}\n\n\n\n".format(item_name, item_icon, item_phase, item_float, item_condition, item_rarity, item_price, item_suggested_price, item_seed, item_inspect))

			# Increment the page counter
			iteration += 1

	# Close the file
	file.close()





if __name__ == '__main__':
	console.clear()

	# Try to take an input as an int, otherwise
	# print the exception and quit
	try:
		pages = int(input("Pages to Scrape: "))
	except Exception as e:
		lu.error(e)
		quit()
	
	# Set a max of 50 pages as bitskins only shows
	# upto 50 pages. If pages is more than 50 then
	# print an arror and quit
	if pages > 50:
		lu.error('Max is 50 pages')
		quit()
	# Otherwise if pages is less than 50, continue
	else:
		parseInspect(pages)