# -*- coding: UTF-8 -*-

from modules.lumodule import *
from bs4 import BeautifulSoup
import requests


lu = lu()


# Get the page html from bitskins
def getHtml(url):
	response = requests.get(url)
	soup = BeautifulSoup(response.content, 'lxml')
	return soup


# Function to parse inspect links
# from HTML obtained from getHtml
def parseInspect(index):
	# Page counter
	iteration = 0
	# Open the file ready to write links
	with open('links.txt', 'a') as file:
		# Iterate through Bitskins pages
		while (iteration < index):
			# Call the getHhtml function and return it Souped
			html = getHtml("https://bitskins.com/?appid=730&page={0}".format(iteration))
			# Find all <a> tags on the page that contain a href
			for a in html.find_all('a', href=True):
				# If part of CS:GO's inspect link is in the tags href
				if ("steam://rungame/730" in str(a)):
					# Log to console the href and 
					lu.success(a['href'])
					# write it to the open file
					file.write(a['href'] + "\n")
			# Increment the page counter
			iteration += 1
	# Close the file
	file.close()





if __name__ == '__main__':
	console.clear()
	parseInspect(int(input("Pages to Scrape: ")))