from modules.lumodule import *
from datetime import datetime
import requests
import time
import json


lu = lu()


# Reads the list specific by the user input and returns it as an array
# If the list does not exist or has no names in it, it will print an
# error/Exception and quit
def readList(list):
	# Try to open the file the user has
	# specified, else on exception, tell
	# the user why the file cannot be opened
	try:
		with open('lists/{0}.txt'.format(list), 'r') as file:
			# Read the file and strip it of whitespace and split each line
			# into a string and store in check_list
			check_list = file.read().strip().splitlines()
			# If the file has more than 0 entries in it,
			# close the file, print how many names are in
			# the file and return the variable the names are
			# stored inside of
			if (len(check_list) > 0):
				file.close()
				lu.info("There are {0} names to check".format(len(check_list)))
				return check_list
			# Otherwise if the file contains 0 entries then
			# tell the user there is 0 entries to check and
			# quit the script
			else:
				lu.error("File {0}.txt contains no names to check")
				quit()
	except Exception as e:
		lu.warn("An error has occurred | {0}".format(e))
		quit()


def requestCheck(name):
	# Store the True of False response from the name exists request and return it
	response = json.loads(requests.get('https://gitlab.com/users/{0}/exists'.format(name)).text)['exists']
	return response
	

def checkNames(names):
	# Create an exception array to store names
	# that cannot be checked due to an error
	exception_array = []

	for name in names:
		try:
			# Try to check if the name does not Exist, if so then
			# log to terminal that the name is available and write
			# it to the open file
			if requestCheck(name) != True:
				lu.success('Username {0} is available!'.format(name))
				available_file.write("{0}\n".format(name))
			# If the name does exist then log to terminal that it
			# already exists and move onto the next name
			else:
				lu.error('Username {0} is not available'.format(name))
		# If the name checking request cannot be sent/read due to an
		# exception, log to terminal the exception and append the
		# unchecked name to the exception_array for reattempting later
		except Exception as e:
			lu.warn(e)
			exception_array.append(name)
			lu.info("Added {0} to exception array".format(name))
			time.sleep(2)
			continue
	# IF the exception array has more then 0 names in
	# it, then attempt to recheck the names in the
	# exception array and reset the array
	if (len(exception_array) > 0):
		checkNames(exception_array)
	# If the exception array has no unchecked names in
	# it then log to console the names have been checked
	else:
	    print()
		lu.success("Successfully checked all names\n\n")





if __name__ == '__main__':
	# Clear the console to be neat
	console.clear()
	# Let the user define which list they would like
	# to check
	lu.info(
		"""
	File must be a .txt file and each name must be on a separate line
	Only enter the files name and not the extension ('.txt')
		""")
	file_name = str(input("lists/"))
	# Try to create an available names file with the current time and
	# file to check as its name, otherwise quit due to an error/exception
	try:
		with open('available/{0}-{1}.txt'.format(datetime.now(), file_name), 'a') as available_file:
			# Call the main name checking function
			checkNames(readList(file_name))
		# After successfully checking all names
		# close the file we created and exit
		available_file.close()
	except Exception as e:
		lu.warn(e)
		quit()