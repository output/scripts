from modules.lumodule import *
from bs4 import BeautifulSoup as suwoop
from datetime import datetime
from colorama import Fore, init
import requests
import json
import random
import names
import time
import string


lu = lu()


init(autoreset=True)


# Reads the list specific by the user input and returns it as an array
# If the list does not exist or has no names in it, it will print an
# error/Exception and quit
def readList(list):
	# Try to open the file the user has
	# specified, else on exception, tell
	# the user why the file cannot be opened
	try:
		with open('lists/{0}.txt'.format(list), 'r') as file:
			# Read the file and strip it of whitespace and split each line
			# into a string and store in check_list
			check_list = file.read().strip().splitlines()
			# If the file has more than 0 entries in it,
			# close the file, print how many names are in
			# the file and return the variable the names are
			# stored inside of
			if (len(check_list) > 0):
				file.close()
				lu.info("There are {0} names to check".format(len(check_list)))
				return check_list
			# Otherwise if the file contains 0 entries then
			# tell the user there is 0 entries to check and
			# quit the script
			else:
				lu.error("File {0}.txt contains no names to check")
				quit()
	except Exception as e:
		lu.warn("An error has occurred | {0}".format(e))
		quit()


def getSessionCookie():
	cookie = requests.get(url="https://protonmail.com/").cookies['Session-Id']
	lu.cookie(cookie)
	return cookie


def requestCheck(sesscookie, name):
	# Proton mail requires headers to be specified 'session-id, and
	# requires exactly the correct 'x-pm-appversion' & 'x-pm--apiversion'
	headers = {
		"Host": "mail.protonmail.com",
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0",
		"Accept": "application/vnd.protonmail.v1+json",
		"Accept-Language": "en-US,en;q=0.5",
		"Accept-Encoding": "gzip, deflate, br",
		"Referer": "https://mail.protonmail.com/create/new?language=en",
		"x-pm-appversion": "Web_3.16.0",
		"x-pm-apiversion": "3",
		"Cache-Control": "no-cache",
		"Pragma": "no-cache",
		"DNT": "1",
		"Connection": "keep-alive",
		"Cookie": "Version=b; Session-Id={0}".format(sesscookie)
	}
	code = json.loads(requests.get(url="https://mail.protonmail.com/api/users/available?Name=" + str(name), headers=headers).text)['Code']
	# Return the name check value, normally either
	# 12106 (unavailable) or 1000 (available)
	return code


def checkNames(names):
	# Create an exception array to store names
	# that cannot be checked due to an error
	exception_array = []
	# A session cookie must be provided to
	# send the name checking request
	session_cookie 	= getSessionCookie()

	for name in names:
		try:
			code = requestCheck(session_cookie, name)
			if (code == 1000):
				lu.success('Username {0} is available!'.format(name))
				available_file.write("{0}\n".format(name))
			elif (code == 12106):
				lu.error('Username {0} is taken'.format(name))
			else:
				lu.warn('Unexpected Code: {0}'.format(code))
		# If the name checking request cannot be sent/read due to an
		# exception, log to terminal the exception and append the
		# unchecked name to the exception_array for reattempting later
		except Exception as e:
			lu.warn(e)
			exception_array.append(name)
			lu.info("Added {0} to exception array".format(name))
			time.sleep(2)
			continue
	# IF the exception array has more then 0 names in
	# it, then attempt to recheck the names in the
	# exception array and reset the array
	if (len(exception_array) > 0):
		checkNames(exception_array)
	# If the exception array has no unchecked names in
	# it then log to console the names have been checked
	else:
		print()
		lu.success("Successfully checked all names\n\n")











if __name__ == '__main__':
	# Clear the console to be neat
	console.clear()
	# Let the user define which list they would like
	# to check
	lu.info(
		"""
	File must be a .txt file and each name must be on a separate line
	Only enter the files name and not the extension ('.txt')
		""")
	file_name = str(input("lists/"))
	print()
	# Try to create an available names file with the current time and
	# file to check as its name, otherwise quit due to an error/exception
	try:
		with open('available/{0}-{1}.txt'.format(datetime.now(), file_name), 'a') as available_file:
			# Call the main name checking function
			checkNames(readList(file_name))
		# After successfully checking all names
		# close the file we created and exit
		available_file.close()
	except Exception as e:
		lu.warn(e)
		quit()